import Logo from "../assets/logo.svg"
import twitter from "../assets/twitter.svg"
import instagram from "../assets/inst.svg"
import facebook from "../assets/facebook.svg"
const createHeader = () => {
    const header = document.createElement('header');
    header.classList.add('header');
    const logo = document.createElement('img');
    logo.src = Logo;
    const headertext = document.createElement('p');
    headertext.textContent = 'WOOCOMMERCE  PRODUCT  SLIDER';
    const social = document.createElement('div');
    social.classList.add('social');
    const twitterLink = document.createElement('a');
    twitterLink.href = '#';
    const twitterImg = document.createElement('img');
    twitterImg.src = twitter;
    twitterLink.appendChild(twitterImg);
    const instLink = document.createElement('a');
    instLink.href = '#';
    const instImg = document.createElement('img');
    instImg.src = instagram;
    instLink.appendChild(instImg);
    const facebookLink = document.createElement('a');
    facebookLink.href = '#';
    const facebookImg = document.createElement('img');
    facebookImg.src = facebook;
    facebookLink.appendChild(facebookImg);
    const cartLink = document.createElement('a');
    cartLink.href = '#';
    cartLink.textContent = 'CART (0)';
    header.appendChild(logo);
    header.appendChild(headertext);
    header.appendChild(social);
    social.appendChild(twitterLink);
    social.appendChild(instLink);
    social.appendChild(facebookLink);
    header.appendChild(cartLink);
    return header;
};export default createHeader;
