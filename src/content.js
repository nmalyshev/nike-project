import cryptokick from "../assets/cryptokick.png"
import arrow from "../assets/arrow.svg"
const createContent = () => {
    const content = document.createElement('content');
    content.classList.add('content');
    const ellipse = document.createElement('button');
    ellipse.classList.add('contentbutton')
    const ellipseImg = document.createElement('img');
    ellipseImg.classList.add('buttonarrow');
    ellipseImg.src = arrow;
    ellipse.appendChild(ellipseImg);
    const shoes = document.createElement('div');
    shoes.classList.add('slider');
    const nikeimg = document.createElement('img');
    nikeimg.classList.add('sliderimg');
    nikeimg.src = cryptokick;
    shoes.appendChild(nikeimg);
    const shoesText = document.createElement('h1');
    shoesText.classList.add('slidertext');
    shoesText.textContent = 'NIKE CRYPTOKICK';
    const shoesText2 = document.createElement('h1');
    shoesText2.classList.add('slidertext','slidertextopacity');
    shoesText2.textContent = 'NIKE CRYPTOKICK';
    shoes.appendChild(shoesText);
    shoes.appendChild(shoesText2);
    const ellipse2 = document.createElement('button');
    ellipse2.classList.add('contentbutton','buttonrev');
    const ellipseImg2 = document.createElement('img');
    ellipseImg2.classList.add('buttonarrow');
    ellipseImg2.src = arrow;
    ellipse2.appendChild(ellipseImg2);
    content.appendChild(ellipse);
    content.appendChild(shoes);
    content.appendChild(ellipse2);
    return content;
};export default createContent;
