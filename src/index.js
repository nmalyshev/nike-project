import '../style.css';
import createHeader from './header';
import createContent from "./content";
import createFooter from "./footer";
const body = document.querySelector('body');
const header = createHeader();
const content = createContent();
const footer = createFooter()
body.appendChild(header);
body.appendChild(content);
body.appendChild(footer);
